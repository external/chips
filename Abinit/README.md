Presentation
------------

ABINIT is a package whose main program allows one to find the total energy, charge density and electronic structure of systems made of electrons and nuclei (molecules and periodic solids) within Density Functional Theory (DFT), using pseudopotentials (or PAW atomic data) and a planewave basis. ABINIT also optimize the geometry according to the DFT forces and stresses, or perform molecular dynamics simulations using these forces, or generate phonons, Born effective charges, and dielectric tensors, based on Density-Functional Perturbation Theory, and many more properties. Excited states and spectra can be computed within the Many-Body Perturbation Theory (the GW approximation and the Bethe-Salpeter equation). DFT+U and Dynamical mean-field theory are available for strongly correlated materials


**Technical information**

* website : http://abinit.org
* Scientific domain : Density Functional Theory 
* Language : C/Fortran
* Parallelism : MPI + OpenMP
* GPU acceleration : Yes (CUDA) 
* Scalability : average
* Vectorization: poor


Compilation and simulation
--------------------------

**Download**

```
./download.sh
```

**Compile**

```
./compile.sh occigen-bdw
```

`machines/occigen-bdw/env_bench` contains the shell environment variables for compiling

**Run and validate the simulation**

For each test case, given in a separate folder (e.g. testcase_XXX), you can find three scripts:

*  prepare.sh: prepare the simulation (move data to the right location, recompile some minor changes, ...)
*  run.sh : run the application and print out the evaluated metric
*  validate.sh: validation of the simulation on a scientific point of view

For running and validating the simulation, one should be able to do:
```
cd testcase_XXX
./prepare.sh occigen-bdw
./run.sh
./validate.sh
```
And getting no error code returned.
Those steps can also be used in a batch file for running the simulation using a job scheduler.




