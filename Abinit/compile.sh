#!/bin/bash
if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls machines/
    echo ""
    echo "Example: ./compile.sh occigen-bdw"
    exit 1
fi
env_file="machines/$1/env_bench"

if [ ! -f $env_file ]
        then
    echo "ERROR: $env_file not found!"
                exit 1
        else
                source $env_file
fi

compile(){
	cd abinit-8.10.3
	make distclean
	env CC="mpicc" CXX="mpicxx" F77="mpif90" F90="mpif90" FC="mpif90" ./configure --disable-debug --enable-mpi --disable-xml --with-linalg-flavor="mkl+scalapack" --with-linalg-libs="-L${MKLROOT}/lib/intel64 -lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64 -mkl" --with-fft-flavor=fftw3  --with-fft-libs="-lfftw3xf_intel -mkl" --with-dft-flavor="libxc+atompaw+wannier90" --enable-mpi-io --with-trio-flavor="netcdf"  --enable-gw-dpc="no" --with-tardir=/tmp/tardir --prefix=$HOME/ABINIT --enable-openmp --enable-optim
	make -j 40
	make install
}

compile
