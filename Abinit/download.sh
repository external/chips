#!/bin/sh
LIST=(
        http://forge.abinit.org/fallbacks/atompaw-4.0.1.0.tar.gz
        http://forge.abinit.org/fallbacks/libxc-3.0.0.tar.gz
        http://forge.abinit.org/fallbacks/netcdf-4.1.1.tar.gz
        https://launchpad.net/wannier90/2.0/2.0.1.1/+download/wannier90-2.0.1.1.tar.gz
	https://www.abinit.org/sites/default/files/packages/abinit-8.10.3.tar.gz
)

copy(){
        cp /store/CINES/dci/SHARED/abs/Abinit/atompaw-4.0.1.0.tar.gz .
        cp /store/CINES/dci/SHARED/abs/Abinit/libxc-3.0.0.tar.gz .
        cp /store/CINES/dci/SHARED/abs/Abinit/netcdf-4.1.1.tar.gz .
        cp /store/CINES/dci/SHARED/abs/Abinit/wannier90-2.0.1.1.tar.gz .
        cp /store/CINES/dci/SHARED/abs/Abinit/abinit-8.10.3.tar.gz . 
}

for name in "${LIST[@]}"; do
        wget --tries=2 --timeout=2 -nc --no-check-certificate $name
        code=$?
	if [[  $code != "0" ]]; then
        	echo "Download failed, try a hard copy:"        
		copy 
                break 2
        fi
done
       
tar xf abinit-8.10.3.tar.gz
