Small test case presentation
----------------------------

**General information**

Small Abinit test case provided by Marc Torrent.
Runs with 8 MPI ranks and 4 OpenMP threads per MPI rank.
Expected duration is a few seconds.

**Case profile**

A profiling on 2 broadwell nodes (64GB) is available in this folder:
"aps_report_occigen.html"

It has been generated using Intel APS (infos: https://software.intel.com/sites/products/snapshots/application-snapshot/) 
