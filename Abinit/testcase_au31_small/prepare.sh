#!/bin/bash

if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls ../machines/
    echo ""
    echo "Example: ./prepare.sh occigen-bdw"
    exit 1
fi
machine_dir="../machines/$1"

rm -f abinit_test_au31_i* abinit_test_au31_o* abinit_test_au31_t*

cp ../machines/${1}/env_bench .
cp ../machines/${1}/batch_small_au31.slurm .
