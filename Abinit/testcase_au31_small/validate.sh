#!/bin/bash
set -e
#set -x
mkdir results-abinit-au31_small
cp -a abinit_test_au31.out* abinit_test_au31.log* abinit_test_au31.files abinit_test_au31.in au.hpc.paw results-abinit-au31_small/

file=abinit_test_au31.out
max_allowed_diff=00000.1
Tot_energy_good=-28668.51038046899999933714

Tot_energy=$(printf -- "%.25g" `grep 'Total energy in eV' $file  | awk '{print $6}'`)
if [ -z "$Tot_energy" ]
then
    echo "bench is not validated"
    exit 1
else
    echo "bench is validated"
    echo $Tot_energy_good $Tot_energy $max_allowed_diff| awk '{if (sqrt(($1 - $2)^2) > $3) print "ERROR Total energy differs more than "$3; else print "OK accurate Total energy"}'
    grep -i "individual time" abinit_test_au31.out | awk -Fwall= '{print "time="$2}'
fi
