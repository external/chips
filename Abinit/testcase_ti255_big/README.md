Big test case presentation
--------------------------

**General information**

Production Abinit test case provided by Marc Torrent.
Runs with 128 MPI ranks and 12 OpenMP threads per MPI rank.
Expected duration is less than one hour.

**Case profile**

A profiling on 64 broadwell nodes (64GB) is available in this folder:
"aps_report_occigen.html"

It has been generated using Intel APS (infos: https://software.intel.com/sites/products/snapshots/application-snapshot/) 
