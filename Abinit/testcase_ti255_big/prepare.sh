#!/bin/bash

if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls ../machines/
    echo ""
    echo "Example: ./prepare.sh occigen-bdw"
    exit 1
fi
machine_dir="../machines/$1"

rm -f abinit_test_ti255_i* abinit_test_ti255_o* abinit_test_ti255_t*

cp ../machines/${1}/env_bench .
cp ../machines/${1}/batch_large_ti255.slurm .
