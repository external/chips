#!/bin/bash
set -e
#set -x 
mkdir results-abinit-ti255_big
cp -a abinit_test_ti255.out* abinit_test_ti255.log* abinit_test_ti255.files abinit_test_ti255.in ti.hpc.paw results-abinit-ti255_big/

file=abinit_test_ti255.out
max_allowed_diff=000000.01
Tot_energy_good=-405774.5462722299999995812

Tot_energy=$(printf -- "%.25g" `grep 'Total energy in eV' $file  | awk '{print $6}'`)
if [ -z "$Tot_energy" ]
then
    echo "bench is not validated"
    exit 1
else
    echo "bench is validated"
    echo $Tot_energy_good $Tot_energy $max_allowed_diff| awk '{if (sqrt(($1 - $2)^2) > $3) print "ERROR Total energy differs more than "$3; else print "OK accurate Total energy"}'
    grep -i "individual time" abinit_test_ti255.out | awk -Fwall= '{print "time="$2}'
fi
