#!/bin/bash

if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls machines/
    echo ""
    echo "Example: ./compile.sh occigen-bdw"
    exit 1
fi
env_file="machines/$1/env_bench"

if [ ! -f $env_file ] 
	then
    echo "ERROR: $env_file not found!"
		exit 1
	else
		source $env_file	
fi

cd XIOS 
./make_xios --prod --arch $COMPILE_ARCH --job 8
cd -
cd DYNAMICO
./make_icosa -prod -parallel $PARALLEL_FLAG -with_xios -arch $COMPILE_ARCH -job 8
mv bin bin_xios
./clean
./make_icosa -prod -parallel $PARALLEL_FLAG -no_io -arch $COMPILE_ARCH -job 8
mv bin bin_noio
./clean
./make_icosa -prod -parallel $PARALLEL_FLAG -arch $COMPILE_ARCH -job 8
cd -
      
      
      
      #If compilation is successfull, you will find in the bin directory the executable file "icosa_gcm.exe"
      
      #./make_icosa -prod -parallel mpi -no_io -arch X64_JEANZAY_PGI_ACC -job 8
      
      #Note that in this case, the openMP compilation must be desactivated.

