#!/bin/bash
export TARBALL="DYNAMICO-XIOS.tar"

if [[ $HOSTNAME = *"occigen"* ]]; then
  tar -xvf /store/CINES/dci/SHARED/abs/DYNAMICO/$TARBALL
elif [ -f $TARBALL ]; then
  tar -xvf $TARBALL
else
  echo "tarball not found"
  exit 1
fi

