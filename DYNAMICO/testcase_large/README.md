Large test case presentation
----------------------------

This case is defined as the RUN_NB640.

It is small run to validate the execution. 
This test is used to evalutate the performance of the computer. May scale up to 80000-100000 cores or up to 640GPUs. 32x32x10=10240 domains are defined. 

Reference time for 10 days simulation on Jean-zay : 
  * CPU No I/O             : 3000s on 5120 cores (128 nodes)
  * GPU No I/O             :  782s on  160 GPUs  (40 nodes)


        
