#!/bin/bash
set -e

mass=`tail -n10 dynamico.out | grep "GLOB "| awk '{print $2}' | awk -F 'E' '{print $2}'`
upper_bound=-11

test=$(awk 'BEGIN{printf ('$mass'<'$upper_bound')?0:1}')
echo $test

if [ $test -eq "0" ] 
	then 
		echo "Validation passed"
	else 
		echo "Validation failed"
		exit 1
fi
mkdir -p results-DYNAMICO-large
cp dynamico.out results-DYNAMICO-large
grep "Time elapsed " dynamico.out 
