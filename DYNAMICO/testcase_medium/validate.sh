#!/bin/bash
set -e

mass=`tail -n10 dynamico.out | grep "GLOB "| awk '{print $2}' | awk -F 'E' '{print $2}'`
upper_bound=-11

test=$(awk 'BEGIN{printf ('$mass'<'$upper_bound')?0:1}')

if [ $test -eq "0" ] 
	then 
		echo "Validation passed"
	else 
		echo "Validation failed"
		exit 1
fi
mkdir -p results-DYNAMICO-medium
cp dynamico.out results-DYNAMICO-medium
grep "Time elapsed " dynamico.out 
