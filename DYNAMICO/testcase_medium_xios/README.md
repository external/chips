Medium XIOS test case presentation
----------------------------------

This case is defined as the RUN_NB40_XIOS.

It is small run to validate the execution. Comparison can be done with or without IO servers to look at the impact on the performance.

Reference time on Jean-zay : 
  * No I/O             : 38s on 800 cores
  * With IO server     : 57s with 800 cores for dynamico and 8 xios servers


Running with XIOS IO servers requiered to execute run in MPMD mode, i.e. with dynamico and xios io server independant executable.

Typically the running command for OpenMPI of type :

mpirun -np 40 icosa_gcm.exe : -np 20 xios_server.exe

IO server processes must be spread all along the node reservation. We advice one IO server by node, shared with icosa_gcm.exe executable.

XIOS is not multithreaded, so the difficulty is to merge on the same node the multithreaded dynamico model. Be carefull about the binding and placement.
       
       

