#!/bin/bash

if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls ../machines/
    echo ""
    echo "Example: ./prepare.sh occigen-bdw"
    exit 1
fi
machine_dir="../machines/$1"

cp -r $machine_dir/env_bench .
cp -r $machine_dir/medium-XIOS .
mv medium-XIOS/batch_medium.slurm .
cp const.def medium-XIOS
