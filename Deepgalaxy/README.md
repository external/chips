Presentation
------------

Deepgalaxy is an AI software. It's goal is to classify image of galaxy into refernces categories. It's Open-source, use CPU or GPU and designed for high performances on super-computers.

**Technical information**

* website : 
* Scientific domain : astrophysics
* Language : Python
* Parallelism : MPI
* GPU acceleration : Yes  
* Scalability : high
* Vectorization: 


Compilation and simulation
--------------------------

**Download**

Sources are available at: 

To download this release, run:

```
./download.sh
```

**Compile**

No compilation because it's interpreted python

**Run and validate the simulation**

For each test case, given in a separate folder (e.g. testcase_medium), you can find three scripts:

*  prepare.sh: prepare the simulation (move data to the right location)
*  run.sh : run the application and print out the evaluated metric
*  validate.sh: validation of the simulation on a scientific point of view

For running and validating the simulation, one should be able to do:
```
cd testcase_XXX
./prepare.sh jean-zay-gpu
./run.sh
./validate.sh
```
And getting no error code returned.
Those steps can also be used in a batch file for running the simulation using a job scheduler.




