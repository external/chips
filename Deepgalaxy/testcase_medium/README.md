Medium test case presentation
-----------------------------

This test case performs a training using 512X512 images, with 3 positions per image, as input.

Reference time on Jean-zay with 4 nodes, 16 GPUs, 3 positions and 100 epochs: 

* For 100epochs: ~67ms/sample and 32min30s as time to solution

Once you have run ``./prepare.sh``, the test case can be run as follows:
```
python dg_train.py --epochs 100 --arch EfficientNetB4 -f output_bw_512.hdf5 --batch-size 4 --noise 0.01 -d "s_1_m_1*" --num-camera 3

```
You can update ``--arch`` as well as ``--batch-size`` in order to get the best performances on the target architecture.
