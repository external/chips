#!/bin/bash

set -e

RESULT_DIR=results-DG-medium
mkdir -p $RESULT_DIR

cp dg.err dg.out $RESULT_DIR


grep "Epoch" -A1 dg.out > $RESULT_DIR/epochs.results
grep "Epoch 100/100" -A1 dg.out > $RESULT_DIR/last_epoch.results
