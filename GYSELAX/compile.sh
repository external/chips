#!/bin/bash

####################################
# Source the right environment
####################################
if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls machines/
    echo ""
    echo "Example: ./compile.sh occigen-bdw"
    exit 1
fi
env_file="machines/$1/env_bench"

if [ ! -f $env_file ]
        then
    echo "ERROR: $env_file not found!"
                exit 1
        else
                source $env_file
                cp machines/$1/*.cmake $GYSELA_PATH/buildsystem/archs/
fi


####################################
# GO TO GYSELAX DIRECTORY
####################################
cd gysela

####################################
# COMPILING
####################################
echo "COMPILING: it can take several minutes"
./scripts/gysmake 1>01.make.out 2>01.make.err
echo "COMPILING: end"

####################################
# TEST THE COMPILATION
####################################
echo "CHECKING THE COMPILATION:"
if test -f "$GYSELA_PATH/wk/gysela.exe"; then
   echo ">> Compilation: SUCCESSFUL"
else
   echo ">> Compilation: FAILED"
   echo ">> Please look at the $GYSELA_PATH/01.make.out "
   echo ">> and $GYSELA_PATH/01.make.err files"
   exit
fi
echo "################################"
