#####################################
## CLONE THE GYSELAX REPO
#####################################
#git clone https://gitlab.maisondelasimulation.fr/gysela-developpers/gysela.git 1>/dev/null 2>/dev/null

####################################
# CLONE THE GYSELAX REPO
####################################
if [[ $HOSTNAME = *"occigen"* ]]; then
  TARBALL_PATH=/store/CINES/dci/SHARED/abs/gyselax
fi
tar xvf $TARBALL_PATH/gysela.tar.gz -C ./ 1>/dev/null 2>/dev/null

####################################
# TEST THE DOWNLOAD
####################################
echo "##########################"
echo "CHECKING THE DOWNLOAD:"
if [ "$(ls -A ./gysela/src/ 2>/dev/null)" ]; then
     echo ">> main directory is not empty - DOWNLOAD SUCCESSFUL"
else
     echo ">> main directory is empty - DONWLOAD FAILED"
     exit
fi 
echo "##########################"
