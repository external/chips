# export ARCH="irene_amd"
# export PROJ="tgcc0088 "

# Module to be loaded:
# module load cmake intel/19.0.5.075 hdf5

# use Intel compiler
set(CMAKE_C_COMPILER icc)
set(CMAKE_Fortran_COMPILER ifort)

# For AMD partition
set(GYS_Fortran_FLAGS_RELEASE       "-g -O3 -diag-disable 5462 -debug none -mavx2 -align all")
#set(GYS_Fortran_FLAGS_RELEASE       "-O3 -diag-disable 5462 -debug none -march=core-avx2 -align all")
#set(GYS_Fortran_FLAGS_RELEASE       "-O3 -align all -diag-disable 5462 -debug none -march=core-avx2 -vec -unroll-aggressive")
set(GYS_Fortran_FLAGS_DETERMINISTIC "${GYS_Fortran_FLAGS_RELEASE} -fp-model source")
set(GYS_Fortran_FLAGS_TIMED         "${GYS_Fortran_FLAGS_RELEASE} -DSYNC_TIMER") #Same as Release
set(GYS_Fortran_FLAGS_DEBUG         "-O0 -g -check bounds -fpe0 -traceback -diag-disable 5462 -mavx2")

option(USE_SYSTEM_YAML 
	"Use an already installed libyaml instead of building the embedded one"	OFF)

set(GYSSUB_ARCH "irene" CACHE STRING "Architecture for which to generate gyssub.")

# Use MKL, not own version for LAPACK
option(OWNLAPACK "Use Gysela Implementation of LAPACK functions." OFF)

