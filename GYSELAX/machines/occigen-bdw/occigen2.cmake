#Config at 17/11/16
#  export ARCH=occigen2
#  module load intel/17.0 openmpi/intel/2.0.1 mkl/17.0 hdf5-seq/1.8.17 cmake git/2.10.2

set(CMAKE_Fortran_COMPILER  ifort)  # if something goes wrong, use mpif90 instead
set(CMAKE_C_COMPILER        icc)    # if something goes wrong, use mpicc instead

# non standard path for libraries
set(CMAKE_PREFIX_PATH "$ENV{HDF5HOME}")

set(GYS_Fortran_FLAGS_RELEASE       "-O3 -xCORE-AVX2 -align all -diag-disable 5462")
set(GYS_Fortran_FLAGS_DETERMINISTIC "${GYS_Fortran_FLAGS_RELEASE} -diag-disable 5462 -fp-model source")
set(GYS_Fortran_FLAGS_TIMED         "${GYS_Fortran_FLAGS_RELEASE} -diag-disable 5462 -DSYNC_TIMER") #Same as Release
set(GYS_Fortran_FLAGS_DEBUG         "-O0 -g -check bounds -fpe0 -traceback -diag-disable 5462 -DQN_COMM_WO_BCAST")

# default (gysela specific) parameters
set(SMT                   ON CACHE BOOL   "Optimize for Simultaneous Multi-Threaded machines")
set(DFLT_HDF5_COMPRESSION "-1" CACHE STRING "Default compression for HDF5 data (-1 for none)")
set(HDF5_USE_EMBEDDED     OFF CACHE BOOL  "Compile our own version of HDF5")