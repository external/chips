Big test TKE presentation
-------------------------

The big TKE case test runs on 30-40 nodes. It runs with 64 MPI tasks and needs ~26 Go (0.4 Go per MPI task).  
To validate the simulation, the value of the ion density is compared. 
