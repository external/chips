#####################################################################
set -e
# VALIDATE THE SIMULATION
#####################################################################
path=`ls -d ./*BIGTEST_TKE*`
rm -rf results

if grep -q "1.401556764165E+08" ${path}/gysela_res.out; then
   echo "TEST HAS PASSED: SUCCESSFUL"
   mkdir results
   cp ${path}/gysela_res* ./results
   cp ${path}/gysela_log* ./results
else
   echo "TEST HAS NOT PASSED: FAIL"
   echo ">>The ion density is not correct"
   exit 1
fi

