GPU test presentation
----------------------------

The GPU case tests corresponds to two tests on 1 and 8 nodes performed on irene AMD that are interesting to port on accelerated nodes.  

Case\_MPI8 on 1 nodes:
 - 8 MPI tasks
 - 8 MPI tasks per node  
 - 14.7 Go per MPI task
 - Total time:  957.56 s
 - Total time (without init & diag): 900.44 s
 
Case\_MPI64 on 8 nodes:
 - 64 MPI tasks
 - 8 MPI per node  
 - 13.7 Go per MPI task
 - Total time:  325.34 s
 - Total time (without init & diag): 230.47 s

The compute time (total time without initialization and diagnostics) is the time indicator for the part of the application targeted to be ported on GPU.
  
The number of OpenMP threads can be set depending on your machine. 
Please, read the section **Add a new machine** in the application [README](../README.md).
