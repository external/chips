####################################
# REMOVE PREVIOUS LOGS
####################################
rm -rf ./*CASE*MPI*
rm -rf ./Case*_MPI*

####################################
# Source the right environment
####################################
if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls machines/
    echo ""
    echo "Example: ./prepare.sh occigen-bdw"
    exit 1
fi
env_file="../machines/$1/env_bench"

if [ ! -f $env_file ]
        then
    echo "ERROR: $env_file not found!"
                exit 1
        else
                source $env_file
                cp ../machines/$1/subgys ./
                cp $GYSELA_PATH/wk/gysela.exe ./
fi

####################################
# COMPILE THE TEST CASE
####################################
if [ ${ARCH} = 'occigen2' ]; then
    echo "No strong scaling for occigen architecture. Only for irene AMD"
elif [ ${ARCH} = 'jeanzay' ]; then
    echo "No strong scaling for Jean-Zay architecture. Only for irene AMD"
elif [ ${ARCH} = 'irene_amd' ]; then
    cp ../machines/irene-amd/Case_MPI8* ./
    cp ../machines/irene-amd/Case_MPI64* ./
    cp ../machines/irene-amd/env_bench ./
fi


