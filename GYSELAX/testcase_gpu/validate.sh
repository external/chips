#####################################################################
# VALIDATE THE SIMULATION
#####################################################################
path=../gysela/wk/

mkdir results
mkdir results/CASE_MPI8
mkdir results/CASE_MPI64

cp ${path}/Case_MPI8_Nthread32_512X1024X64X64X1/gysela_* ./results/CASE_MPI8/

cp ${path}/Case_MPI64_Nthread32_512X1024X64X64X8/gysela_* ./results/CASE_MPI64/
