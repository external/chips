Large test TKE presentation
---------------------------

The scal TKE case test runs on 100-130 nodes. It runs with 256 MPI tasks and needs ~775 Go (3.02Go per MPI task).  
To validate the simulation, the value of the ion density is compared. 
