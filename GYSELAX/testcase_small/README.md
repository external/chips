Small test TKE presentation
---------------------------

The small TKE case test runs on 4-8 nodes. It needs ~3.5 Go (0.4 Go per MPI task).  
To validate the simulation, the value of the ion density is compared. 
