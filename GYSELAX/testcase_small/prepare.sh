#!/bin/bash

####################################
# REMOVE PREVIOUS LOGS
####################################
rm -rf $GYSELA_PATH/wk/*SMALL*TKE*

####################################
# Source the right environment
####################################
if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls machines/
    echo ""
    echo "Example: ./prepare.sh occigen-bdw"
    exit 1
fi
env_file="../machines/$1/env_bench"

if [ ! -f $env_file ]
        then
    echo "ERROR: $env_file not found!"
                exit 1
        else
                source $env_file
                cp ../machines/$1/subgys ./
                cp $GYSELA_PATH/wk/gysela.exe ./
fi

####################################
# COMPILE THE TEST CASE
####################################
if [ ${ARCH} == 'occigen2' ]; then
    cp ../machines/occigen-bdw/smalltest_TKE_A139_n14_dlogTi0.0 ./
    cp ../machines/occigen-bdw/env_bench ./
    sed -i -e "s/aps //" ../gysela/wk/subgys
elif [ ${ARCH} == 'jeanzay' ]; then
    cp ../machines/jean-zay-cpu/smalltest_TKE_A139_n14_dlogTi0.0 ./
    cp ../machines/jean-zay-cpu/env_bench ./
elif [ ${ARCH} == 'irene_amd' ]; then
    cp ../machines/irene-amd/smalltest_TKE_A139_n14_dlogTi0.0 ./
    cp ../machines/irene_amd/env_bench ./
fi
   

