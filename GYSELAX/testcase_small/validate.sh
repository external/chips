#####################################################################
set -e
# VALIDATE THE SIMULATION
#####################################################################
path=`ls -d ./*SMALLTEST_TKE*`
rm -rf results

if grep -q "2.081415648206E+04" ${path}/gysela_res.out; then
   echo "TEST HAS PASSED: SUCCESSFUL"
   mkdir results
   cp ${path}/gysela_res* ./results
   cp ${path}/gysela_log* ./results
else
   echo "TEST HAS NOT PASSED: FAIL"
   echo ">>The ion density is not correct"
   exit 1
fi

