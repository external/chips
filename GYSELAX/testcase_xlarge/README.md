XLarge test TKE presentation
----------------------------

The xlarge TKE case test corresponds to a strong scaling study performed on irene AMD. 3 simulations are performed. 

Case0 on 96 nodes:
 - 768 MPI tasks
 - 8 MPI tasks per node  
 - 4.10 Go per MPI task
 
Case1 on 192 nodes:
 - 1536 MPI tasks
 - 8 MPI per node  
 - 2.25 Go per MPI task
  
Case2 on 384 nodes:
 - 3072 MPI tasks
 - 8 MPI tasks per node
 - 1.64 Go per MPI task

The number of OpenMP threads can be set depending on your machine. 
Please, read the section **Add a new machine** in the application [README](../README.md).
