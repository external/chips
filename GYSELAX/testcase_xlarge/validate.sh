#####################################################################
set -e
# VALIDATE THE SIMULATION
#####################################################################
path=../gysela/wk/

mkdir results
mkdir results/CASE0
mkdir results/CASE1
mkdir results/CASE2
mkdir results/CASE3

cp ${path}/CASE0_MPI768_NTHREAD32_256X512X128X64X48/gysela_res.* ./results/CASE0/
cp ${path}/CASE0_MPI768_NTHREAD32_256X512X128X64X48/gysela_log.* ./results/CASE0/

cp ${path}/CASE1_MPI1536_NTHREAD32_256X512X128X64X48/gysela_res.* ./results/CASE1/
cp ${path}/CASE1_MPI1536_NTHREAD32_256X512X128X64X48/gysela_log.* ./results/CASE1/

cp ${path}/CASE2_MPI3072_NTHREAD32_256X512X128X64X48/gysela_res.* ./results/CASE2/
cp ${path}/CASE2_MPI3072_NTHREAD32_256X512X128X64X48/gysela_log.* ./results/CASE2/

cp ${path}/CASE3_MPI6144_NTHREAD32_256X512X128X64X48/gysela_res.* ./results/CASE3/
cp ${path}/CASE3_MPI6144_NTHREAD32_256X512X128X64X48/gysela_log.* ./results/CASE3/
