Presentation
------------

GROMACS is a versatile package to perform molecular dynamics, i.e. simulate the Newtonian equations of motion for systems with hundreds to millions of particles.
It is primarily designed for biochemical molecules like proteins, lipids and nucleic acids that have a lot of complicated bonded interactions, but since GROMACS is extremely fast at calculating the nonbonded interactions (that usually dominate simulations) many groups are also using it for research on non-biological systems, e.g. polymers.

**Technical information**

* website : http://www.gromacs.org
* Scientific domain : molecular dynamics
* Language : C/C++/CUDA
* Parallelism : MPI + OpenMP + CUDA
* GPU acceleration : yes  
* Scalability : med
* Vectorization: high

MPI-Bound Code


Compilation and simulation
--------------------------

**Download**

Sources are available at: http://manual.gromacs.org/documentation

For the test, we will use a specific release. To download this release, run:

```
./download.sh
```

**Compile**

Gromacs needs cmake (works with 3.9.1) and python (2.7) to be compiled and depends on a version of fftw (works with fftw from mkl version 18)

Compile the code using for instance:
```
./compile.sh occigen-bdw
```

`machines/occigen-bdw/env` contains the information for compilation (module load gcc openmpi lapack hdf5 ...)

You can create your own machine directory under `machines` to define the appropriate environment.
Information (if needed) about how to compile the application. 

**Run and validate the simulation**

For each test case, given in a separate folder (e.g. testcase_ion_channel_small, testcase_ion_channel_large and testcase_BSG_large), you can find three scripts:


1.  prepare.sh: prepare the simulation (move data to the right location, recompile some minor changes, ...)
2.  run.sh : run the application and print out the evaluated metric in a file called results (first numeric column representing ns/day corresponds to the evaluated metric to take into account)
3.  validate.sh: validation of the simulation. 

*  The bench testcase_ion_channel_small is considered validated if:
    * the evaluated metric is print out ;
    * the simulation is completed with  20 000 steps ;
    * the files small_ion_channel.log, small_ion_channel.edr, results and slurm*.out are present in the directory results-gromacs-ion_channel_small.
*  The bench testcase_ion_channel_large is considered validated if:
    *  the evaluated metric is print out ;
    *  the simulation is completed with 1 000 000 steps ;
    *  the files large_ion_channel.log, large_ion_channel.edr, results and slurm*.out are present in the directory results-gromacs-ion_channel_large.
*  The bench testcase_BSG_large is considered validated if:
    *  the evaluated metric is print out ;
    *  the simulation is completed with 1 200 000 steps ;
    *  the files benchBSG.cpt, benchBSG.trr, benchBSG.xtc, benchBSG.log, benchBSG.edr, results and slurm*.out are present in the directory results-gromacs-BSG_large.


For running and validating the simulation, one should be able to do:
```
cd testcase_XXX
./prepare.sh compile_dir_name (ex : ./prepare.sh compile_02-07-20-09-45-52)
./run.sh run_dir_name (ex : ./run.sh run_02-07-20-09-52-08_compile_02-07-20-09-45-52)
./validate.sh run_dir_name (ex : ./validate.sh run_02-07-20-09-52-08_compile_02-07-20-09-45-52)
```
And getting no error code returned.
Those steps can also be used in a batch file for running the simulation using a job scheduler.

