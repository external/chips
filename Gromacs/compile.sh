#!/bin/bash
export software=gromacs
export version=2020.1

Untar(){
	echo "======== untar package in ======"
	pwd
	tar -zvxf  $software-${version%%-*}.tar.gz
}

Install_bin(){
	echo "======== install bin ========"
	install_dir=${software}/${version%%-*}
	mkdir -p $install_dir
	cd $software-${version%%-*}
  rm -rf build
  env > env_before_start.out 
	mkdir build
	cd build
	echo "
	cmake ../  -DGMX_MPI=${CFL_GROMACS_MPI} -DGMX_OPENMP=${CFL_GROMACS_OPENMP} -DGMX_GPU=${CFL_GROMACS_GPU} -DCMAKE_INSTALL_PREFIX=../../$install_dir -DGMX_X11=OFF -DBUILD_SHARED_LIBS=OFF -DCMAKE_C_FLAGS="${CFL_GROMACS_DCMAKE_C_FLAGS}" -DCMAKE_CXX_FLAGS="$CFL_GROMACS_DCMAKE_CXX_FLAGS" -DGMX_FFT_LIBRARY="${CFL_GROMACS_DGMX_FFT_LIBRARY}" -DMKL_INCLUDE_DIR="${CFL_GROMACS_DMKL_INCLUDE_DIR}" -DCMAKE_CXX_COMPILER=${mympicxxcomp} -DCMAKE_C_COMPILER=${mympiccomp} "  1>compile.out 2>compile.err
	cmake  ../  -DGMX_MPI=${CFL_GROMACS_MPI} -DGMX_OPENMP=${CFL_GROMACS_OPENMP} -DGMX_GPU=${CFL_GROMACS_GPU} -DCMAKE_INSTALL_PREFIX=../../$install_dir -DGMX_X11=OFF -DBUILD_SHARED_LIBS=OFF -DCMAKE_C_FLAGS="${CFL_GROMACS_DCMAKE_C_FLAGS}" -DCMAKE_CXX_FLAGS="${CFL_GROMACS_DCMAKE_CXX_FLAGS}" -DGMX_FFT_LIBRARY="${CFL_GROMACS_DGMX_FFT_LIBRARY}" -DMKL_INCLUDE_DIR="${CFL_GROMACS_DMKL_INCLUDE_DIR}" -DCMAKE_CXX_COMPILER=${mympicxxcomp} -DCMAKE_C_COMPILER=${mympiccomp} 1>>compile.out 2>>compile.err
make -j 40 install 1>>compile.out 2>>compile.err
}

Deploy(){
    Untar
    Install_bin
}

if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls machines/
    echo ""
    echo "Example: ./compile.sh occigen-bdw"
    exit 1
fi
env_file="machines/$1/env_bench"

if [ ! -f $env_file ] 
	then
    echo "ERROR: $env_file not found!"
		exit 1
	else
		source $env_file	
fi
Deploy
