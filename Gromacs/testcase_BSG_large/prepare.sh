#!/bin/bash

if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls ../machines/
    echo ""
    echo "Example: ./prepare.sh occigen-bdw"
    exit 1
fi
machine_dir="../machines/$1"

cp ${machine_dir}/batch_large_BSG.slurm batch_large_BSG.slurm.tmp.tmp
awk '$1=$1' FS="GROMACS_HOME" OFS="../gromacs/2020.1" batch_large_BSG.slurm.tmp.tmp > batch_large_BSG.slurm.tmp
awk '$1=$1' FS="GROMACS_HOME" OFS="../gromacs/2020.1" batch_large_BSG.slurm.tmp >  batch_large_BSG.slurm

rm -f  batch_large_BSG.slurm.tmp.tmp batch_large_BSG.slurm.tmp
