#!/bin/bash
set -e

perf=`grep Performance: results | awk '{ print $2 }'`
end_sim=`grep "Statistics over 1200001 steps using 12001 frames" benchBSG.log`

mkdir results-gromacs-BSG_large
cp benchBSG.* results-gromacs-BSG_large/.
cp slurm* results-gromacs-BSG_large/.
cp results results-gromacs-BSG_large/.
cd results-gromacs-BSG_large

if [ -z "$perf" ] || [ -z "$end_sim" ]
then
    echo "bench is not validated"
    exit 1
else
    echo bench is validated
    echo end = $end_sim
    echo perf = $perf ns/day
fi
