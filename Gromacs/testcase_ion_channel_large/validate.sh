#!/bin/bash
set -e
perf=`grep Performance results | awk '{ print $2 }'`
end_sim=`grep "Statistics over 1000001 steps using 100001 frames" large_ion_channel.log`

mkdir results-gromacs-ion_channel_large
cp large_ion_channel* results-gromacs-ion_channel_large/.
cp slurm* results-gromacs-ion_channel_large/.
cp results results-gromacs-ion_channel_large/.
cd results-gromacs-ion_channel_large

if [ -z "$perf" ] || [ -z "$end_sim" ]
then
    echo "bench is not validated"
    exit 1
else
    echo bench is validated
    echo end = $end_sim
    echo perf = $perf ns/day
fi
