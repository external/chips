#!/bin/bash
set -e
perf=`grep Performance results | awk '{ print $2 }'`
end_sim=`grep "Statistics over 20001 steps using 2001 frames" small_ion_channel.log`

mkdir results-gromacs-ion_channel_small
cp small_ion_channel* results-gromacs-ion_channel_small/.
cp slurm* results-gromacs-ion_channel_small/.
cp results results-gromacs-ion_channel_small/.
cd results-gromacs-ion_channel_small

if [ -z "$perf" ] || [ -z "$end_sim" ]
then
    echo "bench is not validated"
    exit 1
else
    echo bench is validated
    echo end = $end_sim
    echo perf = $perf ns/day
fi
