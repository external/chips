#!/bin/bash
if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls machines/
    echo ""
    echo "Example: ./compile.sh occigen-bdw"
    exit 1
fi
env_file="machines/$1/env_bench"

if [ ! -f $env_file ] 
	then
    echo "ERROR: $env_file not found!"
		exit 1
	else
		source $env_file	
fi

cp machines/$1/Makefile.inc MUMPS_5.2.1/Makefile.inc
cd MUMPS_5.2.1
make clean
make -j 20

cd ..
make
