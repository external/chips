Scale test case presentation
----------------------------

The scale test case is running from 1 to 32 system resolutions using 200 nodes. 

Each instance of system resolution is distributed over the 200 nodes, using one MPI per node per instance, and as many OpenMP thread as possible.  

