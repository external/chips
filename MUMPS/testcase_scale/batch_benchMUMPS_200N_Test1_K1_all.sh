#!/bin/bash               
#SBATCH -J mumpsben
#SBATCH --nodes=200
#SBATCH --tasks-per-node=12
#SBATCH --threads-per-core=1
#SBATCH --cpus-per-task=2
#SBATCH --exclusive
#SBATCH --output outputs/output_mumpsbench_%J
#SBATCH --time=06:00:00
#SBATCH -C BDW28|HSW24

module purge
module load intel
module load intelmpi
module load parmetis/4.0.3-real64
module rm intel
module load intel/19.4
module list

set -x

ldir=.
outputdir=outputs
mumpstest=mumps_bench

export OMP_NUM_THREADS=2

# Run with K=1 (see documentation in mumps_bench.F)
K=1
envtest="in_${SLURM_NNODES}Nodes_K=${K}_Test1_ALL.txt"
input=InputFiles/${envtest}
output=$outputdir/output_${envtest}_${SLURM_JOBID}.log
echo "RUN $envtest"
time srun -m block:cyclic ./${mumpstest} < ${input}   >& ${output}
