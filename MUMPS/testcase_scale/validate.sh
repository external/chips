#!/bin/bash
set -e

#set -x 
mkdir -p results-mumps-scale
cp -a output_mumpsbench output_*log input.txt ../mumps_bench results-mumps-scale/

K=1
envtest="input.txt"
file="output_*.log"
max_allowed_diff=0.1
operations_in_node_elimination_good=63980000000000

operations_in_node_elimination=$(printf -- "%.25g" `grep 'Operations in node elimination' $file  | sort -u |  awk '{print $7}' | tr 'D' 'E'`)

echo $operations_in_node_elimination_good $operations_in_node_elimination $max_allowed_diff| awk '{if (sqrt(($1 - $2)^2) > $3) print "ERROR Operations in node elimination differs more than "$3; else print "OK accurate Operations in node elimination"}'
echo $operations_in_node_elimination_good $operations_in_node_elimination $max_allowed_diff| awk '{if (sqrt(($1 - $2)^2) > $3) print "ERROR Operations in node elimination differs more than "$3; else print "OK accurate Operations in node elimination"}' >> exitornot

max_allowed_diff=0.0
flops_difference_good=0.0
flops_difference=$(printf -- "%.25g" `grep "FLOPS_REFERENCE DIFFERENCE" results-mumps-scale/output_*.log  | awk '{print $4}' | sort -u | sort -n -k1 | tail -n 1`)
echo $flops_difference_good $flops_difference $max_allowed_diff| awk '{if (sqrt(($1 - $2)^2) > $3) print "ERROR flops differs more than "$3; else print "OK accurate flops"}'
echo $flops_difference_good $flops_difference $max_allowed_diff| awk '{if (sqrt(($1 - $2)^2) > $3) print "ERROR flops differs more than "$3; else print "OK accurate flops"}' >> exitornot

# linked to set -e, if the grep doesn't work, the script will fail
grep "OK accurate Operations in node elimination" exitornot
grep "OK accurate flops" exitornot
rm exitornot

grep "BENCH: TIME" $file  | grep "simultaneous instances" | awk -F= '{print $2" instances time"$3}'
