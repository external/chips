Presentation
------------

**Technical information**

* Scientific domain :climate
* Language : Fortran
* Parallelism : MPI + 
* GPU acceleration : No
* Scalability : high
* Vectorization: moderate


Compilation and simulation
--------------------------

**Download**

In order to get the sources, please run:


```
./download.sh
```

**Compile**

Compile the code using for instance:
```
./compile.sh jean-zay-cpu
```

`machines/jean-zay-cpu/env` contains the information for compilation (module load gcc openmpi lapack hdf5 ...)

You can create your own machine directory under `machines` to define the appropriate environment.


**Run and validate the simulations**

For each test case, given in a separate folder (e.g. testcase_small), you can find three scripts:

*  prepare.sh: prepare the simulation (move data to the right location, recompile some minor changes, ...)
*  run.sh : run the application and print out the evaluated metric
*  validate.sh: validation of the simulation on a scientific point of view

For running and validating the simulation, one should be able to do:
```
cd testcase_XXX
./prepare.sh jean-zay-cpu
./run.sh
./validate.sh
```
And getting no error code returned.
Those steps can also be used in a batch file for running the simulation using a job scheduler.
