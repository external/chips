#!/bin/bash

if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls machines/
    echo ""
    echo "Example: ./compile.sh occigen-bdw"
    exit 1
fi
env_file="machines/$1/env_bench"

if [ ! -f $env_file ] 
	then
    echo "ERROR: $env_file not found!"
		exit 1
	else
		source $env_file	
fi

#xios-2.5 directory must be a sub-directory of NEMO/release-4.0
# 1. Prepare an arch file for the target machine 
#cp machines/$1/arch-XIOS.fcm xios-2.5/arch/arch-$1.fcm
#cd xios-2.5
#./make_xios --arch $1 -j 8
#cd -

#NEMO
#create a configuration file for the targeted machine ( i.e. irene) arch-irene.fcm
# lancement de la compilation pour BENCH
cp machines/$1/arch-NEMO.fcm release-4.0/arch/arch-$1.fcm
cd release-4.0
  ./makenemo -m $1 -a BENCH -v3
#
# Find the produced exec 
#
# find . -name nemo.exe
# ./tests/BENCH/BLD/bin/nemo.exe
#



