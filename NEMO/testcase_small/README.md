Small test case presentation
----------------------------

This test is used as a standard NEMO benchmark to evalutate the performance of the computer. May scale up to about 4k cores.


Five configurations can be performed for 20000 steps each


```
1560 tasks ; jpni=52, jpnj=30
1920 tasks ; jpni=40, jpnj=48
2160 tasks ; jpni=45, jpnj=48
2640 tasks ; jpni=40, jpnj=66
3960 tasks ; jpni=60, jpnj=66

```

Those values can be changed within the batch script (after running the prepare.sh script) to run on the different ocnfigurations.

The performance obtained can be seen after running validate.sh in results-NEMO_small/elapsed_and_SYPD.out
E.g.:
```
cat results-NEMO_small/elapsed_and_SYPD.out
Max time elapsed is: 1044.062710s, SYPD is 188.916, for rank:3090
```
