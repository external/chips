#!/bin/bash

set -e

RESULT_DIR=results-NEMO_small
mkdir -p $RESULT_DIR

cp *namelist* nemo layout.dat time.step timing.output ocean.output communication_report.txt $RESULT_DIR


echo "Elapsed time, SYPD, MPI rank" > $RESULT_DIR/results_per_rank.csv
grep SYPD timing.output | awk '{print $4, $6, $12}' >> $RESULT_DIR/results_per_rank.csv

cat $RESULT_DIR/results_per_rank.csv | awk '{print "Max time elapsed is: "$1"s, SYPD is "$2", for rank:" $3}' | sort | tail -n2 | head -n1 > $RESULT_DIR/elapsed_and_SYPD.out
