Presentation
------------
Ramses is an open source code to model astrophysical systems, featuring self-gravitating, magnetised, compressible, radiative fluid flows. It is based on the Adaptive Mesh Refinement (AMR) technique on a fully-threaded graded octree. ramses is written in Fortran 90 and is making intensive use of the Message Passing Interface (MPI) library.

**Technical information**

* website : https://www.ics.uzh.ch/~teyssier/ramses/RAMSES.html
* Scientific domain : Astrophysics
* Language : Fortran 90
* Parallelism : MPI 
* GPU acceleration : No
* Scalability : High 
* Vectorization: Medium


Compilation and simulation
--------------------------

Here we describe the different phases from the download to the validation of the simulation.

**Download**

The ramses version used is based on the stable\_18\_09 version (see git bitbucket https://bitbucket.org/rteyssie/ramses). Home-made modifications (made by the ramses developers) have been made to be able to start from an evolved system, and make a verifiable benchmark (currently impossible with the official). The patch concerns inputs and outputs and routines representing less than 1% of the computing time. In the official distributions, on large simulations the i/o parameters are written in the checkpoints and are not read back in the namelist. 

More informations can be found directly on https://bitbucket.org/rteyssie/ramses/overview

1. The first step, if the input and the test case are not available on the machine, is to download ramses, inputs and  tests cases :

~~~
    $ ls -l abs/ramses/
    total 4397808348
    -rwxr-x---. 1 X X 3922439168000 Feb  7 15:12 input_big.tar
    -rwxr-xr--. 1 X X  353831936000 Feb  7 13:25 input.tar
    -rwx--x---. 1 X X      20480000 Feb 10 11:36 PROD_BIG.tar
    -rwx--x---. 1 X X      20480000 Feb 10 11:36 PROD.tar
    -rwxr-xr--. 1 X X       6424389 Jan 15 14:40 ramses.tar.gz
    -rwxr-xr--. 1 X X   58599002085 Feb  7 13:15 testcase_small.tar.gz

~~~

with:

* input_big.tar: Large test case
* input.tar: Medium test case
* PROD_BIG.tar: Contains the namelist used for the large test case
* PROD.tar: Contains the namelist used for the medium test case
* ramses.tar.gz: The ramses version patched as explained above
* testcase_small.tar.gz: Small test case with the namelist inside

2. The second step is to edit env_bench script (in RAMSES/machines/$machine-name/) which include:
* bench_dir: root directory of the bench folder
* environments variables if needed (like MPIF90, F90, ...)

3. Then, you can use the following script to untar the pre-imported/pre-downloaded version of ramses.
```
./download.sh
```

**Compile**

Compile the code using for instance:
```
./compile.sh occigen-bdw
```

`machines/occigen-bdw/env` contains the information for compilation (module load gcc openmpi lapack hdf5 ...)

You can create your own machine directory under `machines` to define the appropriate environment.

**Run and validate the simulation**

For each test case, given in a separate folder (e.g. testcase_small), you can find three scripts:

*  prepare.sh: prepare the simulation (move data to the right location)
*  run.sh : run the simulation
*  validate.sh: validation of the simulation from a technical and scientific point of view

To run and validate the simulation, the following commands must be executed:
```
cd testcase_XXX
./prepare.sh occigen-bdw
./run.sh
./validate.sh
```
And getting no error code returned.

***Run:***
- Specific informations about jobs to be launched can be found in the READ.md file 
of each test case (testcase_small, testcase_medium and testcase_large).

***Validation:***
- **Validation Metric**: The metric used for this bench is the **total time of the simulation 
in seconds excluding initialization and I/Os**. (For more details see the validate.sh 
script). 
- **Validation files**: Name of the log file of the simulation, validation measure
*dt* as well as the total simulation time are written in the *file metric-$TEST_NAME.log*
then copied into the folder *$TEST_NAME/results-$SOFT_NAME-$TEST_NAME*, log file 
of the simulation and also copied into this folder.
- **Physical validation**: the variable used for *physical validation* is the *dt* 
value found at the end of the simulation, i.e. at the end of the simulation log file.


Here is an example of the metric file
```
$ more testcase_medium/resluts-RAMSES-medium/metric-medium.log
log file: run_03-05-20-18-22-20.log
end = 706.754295588002 s (total simulation time)
perf = 687.3888909350029 s (total simulation time excluding initialization and i/o)
***********************************************************************************
```

**More information about the dt value**:

*dt* is a calculated value that incorporates the temperature, density and gas velocity 
of all the cells in the simulation. This value also depends on the size of the cells 
(i.e. the size of the mesh) and therefore depending on the granularity of the mesh 
the dt value will be greater or smaller. For example in the small test case (A.K.A DEBUG) 
dt will be of the order of 10^-1 to 10^-2 because the mesh used are relatively large 
while for the other two cases this value will fluctuate a few tens of percent around 
10^-5. In fact, in ramses the errors get bigger and the value beyond 10^-7 or 10^-3 
the calculations are clearly wrong. Finally, depending on the machine used, the 
value dt will not be exactly the same because to calculate dt, the values pass 
through an MPI_Allreduce depending on the MPI library used.
