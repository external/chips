#!/bin/bash
if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls machines/
    echo ""
    echo "Example: ./compile.sh occigen-bdw"
    exit 1
fi
env_file="machines/$1/env_bench"

if [ ! -f $env_file ] 
	then
    echo "ERROR: $env_file not found!"
		exit 1
	else
		source $env_file	
fi

compile(){
    cd ./ramses/bin
    make clean
    rm ramses3d
    # FFLAGS
    if [[ $HOSTNAME = *"irene"* ]]; then
         sed -i s/"FFLAGS = -cpp -DNDIM=\$(NDIM) -DNPRE=\$(NPRE) -DSOLVER\$(SOLVER) -DNVAR=\$(NVAR) -DNCHEM=\$(NCHEM)"/"FFLAGS = -mavx2 -O3 -ipo -no-prec-div -cpp -DNDIM=\$(NDIM) -DNPRE=\$(NPRE) -DSOLVER\$(SOLVER) -DNVAR=\$(NVAR) -DNCHEM=\$(NCHEM)"/g  ./Makefile
    else
    	sed -i s/"FFLAGS = -cpp -DNDIM=\$(NDIM) -DNPRE=\$(NPRE) -DSOLVER\$(SOLVER) -DNVAR=\$(NVAR) -DNCHEM=\$(NCHEM)"/"FFLAGS = -xHost -O3 -ipo -no-prec-div -cpp -DNDIM=\$(NDIM) -DNPRE=\$(NPRE) -DSOLVER\$(SOLVER) -DNVAR=\$(NVAR) -DNCHEM=\$(NCHEM)"/g  ./Makefile
    fi
    if [[ $HOSTNAME = *"jean-zay"* ]]; then
        sed -i s/"F90 = mpif90"/"F90 = mpiifort"/g ./Makefile    
    fi
    pwd
    make >& make.log
    if [ -z "ramses3d" ]; then
        echo "ramses3d binary not found/compiled"
        exit 1
    else 
        echo "Compilation of ramses3d succeeded"
    fi
}

compile
