#!/bin/bash

git_download(){
    git clone https://bitbucket.org/rteyssie/ramses
    cd ramses
    git checkout stable_19_10 # Checkout a stable version
}

tar_download(){
	if [[ $HOSTNAME = *"occigen"* ]]; then
		source machines/occigen-bdw/env_bench
		tar zxvf /store/CINES/dci/SHARED/abs/ramses/ramses.tar.gz 
	elif [[ $HOSTNAME = *"jean-zay"* ]]; then
		source machines/jean-zay-cpu/env_bench
		tar zxvf $SCRATCH/abs/ramses/ramses.tar.gz 
	elif [[ $HOSTNAME = *"irene"* ]]; then
		source machines/irene-amd/env_bench
		tar zxvf  $CCCSCRATCHDIR/abs/ramses/ramses.tar.gz 
	else
		echo "Hostname not recognized: abort"
		exit 1
	fi
}
tar_download
