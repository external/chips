Large test case presentation
----------------------------

**General information**
To obtain a representative case of a real astrophysical problem, **we start from an evolved case**, i.e. the code has already run (more than 24 hours in this case) and the code starts again from restart files located in a folder named necessarily "input", because the ramses executable is implemented this way (these restart files are unarchived in the "prepare.sh" phase).

The code **cannot change the number of MPI processes** of the initial simulation when it starts from a checkpoint/restart, so **the number of processes is fixed**.

For this case, we will use **12800 MPI** processes, and we will need about **3.5-3.6GB** of memory per MPI process. To simplify we will take 1 core per MPI process on machines with nodes with enough memory and we will depopulate when necessary. For example we will use 320 nodes for Jean-Zay, 40 tasks per node, i.e one task per cpus.

**/!\ On AMD Rome** bi-processor compute nodes, 64 cores/proc with 256 GB **you will need to use 2 cores per MPI process to avoid memory overflow.** So we will use 200 compute nodes.
