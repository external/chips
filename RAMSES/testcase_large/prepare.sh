#!/bin/bash
if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls ../machines/
    echo ""
    echo "Example: ./prepare.sh occigen-bdw"
    exit 1
fi
machine_dir="../machines/$1"

cp $machine_dir/env_bench .
cp $machine_dir/batch_large.slurm .

echo "************************************************************"
echo "************************************************************"
echo "* Prepare Large case in $bench_dir"
echo "************************************************************"
if [[ $HOSTNAME = *"occigen"* ]]; then
	mkdir -p $bench_dir/testcase_large/
	#lfs setstripe -c 60 . 
	tar xvf /store/CINES/dci/SHARED/abs/ramses/input.tar.gz 
	tar xvf /store/CINES/dci/SHARED/abs/ramses/PROD.tar 
elif [[ $HOSTNAME = *"jean-zay"* ]]; then
	tar xvf $SCRATCH/abs/ramses/input_big.tar 
	tar xvf $SCRATCH/abs/ramses/PROD_BIG.tar 
elif [[ $HOSTNAME = *"irene"* ]]; then
	#lfs setstripe -c 60 .  # ? squareroot(3600)=60
        tar xvf  $CCCSCRATCHDIR/abs/ramses/input_big.tar
	tar xvf  $CCCSCRATCHDIR/abs/ramses/PROD_BIG.tar
else
	echo "Hostname not recognized: abort"
	exit 1
fi
cp ./PROD_BIG/cosmo.nml .
echo "************************************************************"
echo "* End Prepare large case "
echo "************************************************************"
