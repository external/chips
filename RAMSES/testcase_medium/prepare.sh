#!/bin/bash
if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls ../machines/
    echo ""
    echo "Example: ./prepare.sh occigen-bdw"
    exit 1
fi
machine_dir="../machines/$1"

cp $machine_dir/env_bench .
cp $machine_dir/batch_medium.slurm .
#lfs setstripe -c 18 .  # ? squareroot(330)~18

echo "************************************************************"
echo "************************************************************"
echo "* Prepare medium case in $bench_dir"
echo "************************************************************"
if [[ $1 = "occigen-bdw" ]]; then
	tar xvf /store/CINES/dci/SHARED/abs/ramses/input.tar.gz
	tar xvf /store/CINES/dci/SHARED/abs/ramses/PROD.tar
elif [[ $1 = "jean-zay-cpu" ]]; then
	tar xvf $SCRATCH/abs/ramses/input.tar
	tar xvf $SCRATCH/abs/ramses/PROD.tar
elif [[ $1 = "irene-amd" ]]; then
	tar xvf  $CCCSCRATCHDIR/abs/ramses/input.tar
	tar xvf  $CCCSCRATCHDIR/abs/ramses/PROD.tar
else
	echo "Hostname not recognized: abort"
	exit 1
fi

cp ./PROD/cosmo.nml .

echo "************************************************************"
echo "* End Prepare medium case "
echo "************************************************************"
