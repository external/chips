#!/bin/bash
set -e
source ./env_bench
if [[ $HOSTNAME = *"occigen"* ]]; then
	module switch intel/18.1 intel/17.2 
	module load python/3.6.3
elif [[ $HOSTNAME = *"jean-zay"* ]]; then
	module load python/3.7.5
elif [[ $HOSTNAME = *"irene"* ]]; then
	module load python3/3.7.5
else
        echo "Hostname not recognized: abort"
        exit 1
fi
echo "**********************************************************************************************************"
echo "* Validate medium case *"
echo "**********************************************************************************************************"
echo "* ls -1 run.log" && ls -1 run.log
log_file=./run.log
start_t=`grep startup $log_file | awk '{ print $5 }'`
end_t=`grep "Total elapsed time:"  $log_file | awk '{ print $4 }'`
perf=`bc -l <<< $end_t-$start_t`

if [ -z "$perf" ] || [ -z "$end_t" ]
then
    echo "****************************************"
    echo "* NOK -> Medium bench is not validated *"
    echo "****************************************"
    exit 1
else
    echo "***********************************"
    echo "* => OK Medium bench is validated *"
    echo "***********************************"
    a=`grep $log_file metric-medium.log | wc -l`
    if [ "$a" -lt "1" ]; then
	echo "log file: $log_file" >> metric-medium.log
        echo "end = $end_t s (total simulation time)" >> metric-medium.log
        echo "perf = $perf s (total simulation time excluding initialization and i/o)" >> metric-medium.log
        echo "***********************************************************************************" >> metric-medium.log
    else
        echo "Metrics already written in metric-medium.log"
    fi
fi
echo "***********************************"
echo "* Physical validation Medium case *"
echo "***********************************"
dt=`grep -B 1 "Run completed" $log_file | grep dt | awk -F ' ' '{print $6}'`
exp_simu=`grep -B 1 "Run completed" $log_file | grep dt | awk -F ' ' '{print $6}' | awk -F 'E' '{print $2}'` 
echo "dt=" $dt
echo "exponent="$exp_simu
exp_ref=-5 # Defined and validated with 
abs_tol=1  # minimum absolute tolerance
python3 ../tools/isclose.py $exp_ref $exp_simu $abs_tol
# Copy log file 
mkdir -p results-RAMSES-medium
cp $log_file metric-medium.log results-RAMSES-medium/. 
tail -n 4 results-RAMSES-medium/metric-medium.log
