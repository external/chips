Small test case presentation
----------------------------

**General information**
For this simulation, we start from a **non-evolved initial condition** (called IC_l9) and therefore not really representative of a real case.
The small test case (A.K.A Debug test case) of ramses is a **debugging case** that runs in **less than 10 minutes** on **256 cores**. 

**Debbuging**
To allow debugging on a variable number of cores, we lauch the simulation from a non-evolved system. So, if you need to perform a function/debug test, you can use this test case to change the number of cores and nodes. If necessary, you can reduce the memory footprint, for this we need to reduce "ngridmax" in the name list cosmo.nml to 300000 instead of 400000 and npartmax to 1700000 instead of 2000000 (for example).

Please use 256 MPI and cores in the context of the benchmark.

**Warning**
Don't worry about messages like "File IC_l9/ic_tempb not found" in the .log file : it doesn't find some files => it switches to another type of initial conditions, and everything goes well, they are not errors.

**Some metrics**
- TGCC/Joliot Curie AMD:  468 seconds on 256 cores () (January 2020)
- Occigen/Haswell nodes: 563 seconds on 256 cores (11 nodes) (January 2020)
- Occigen/Broadwell nodes: 481 seconds on 256 cores (10 nodes) (february 2020)
- Jean-Zay-cpu/Cascade Lake nodes: 545 seconds on 256 cores (7 nodes) (february 2020)

**Case profile**

A profiling of a small Ramses test case performed on Occigen on 1 haswell node (64GB) is available in this folder:
`profile_occigen-hsw.html"

It has been generated using Intel APS (infos: https://software.intel.com/sites/products/snapshots/application-snapshot/) 
