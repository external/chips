#!/bin/bash
if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls ../machines/
    echo ""
    echo "Example: ./prepare.sh occigen-bdw"
    exit 1
fi
machine_dir="../machines/$1"

cp $machine_dir/env_bench .
cp $machine_dir/batch_small.slurm .

echo "************************************************************"
echo "* Copy small case "
echo "************************************************************"
if [[ $1 = "occigen-bdw" ]]; then
	tar zxvf /store/CINES/dci/SHARED/abs/ramses/testcase_small.tar.gz 
elif [[ $1 = "jean-zay-cpu" ]]; then
	tar zxvf $SCRATCH/abs/ramses/testcase_small.tar.gz 
elif [[ $1 = "irene-amd" ]]; then
	tar zxvf  $CCCSCRATCHDIR/abs/ramses/testcase_small.tar.gz 
else
	echo "Hostname not recognized: abort"
	exit 1
fi
echo "************************************************************"
echo "* End Prepare small case "
echo "************************************************************"
