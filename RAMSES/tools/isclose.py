#!/usr/bin/python
import sys
import os
import math

def main():
	if len(sys.argv) != 4:
		print("Please provide 3 arguments as follow")
		print("This snippet use math.isclose function of python v3.6")
		print("Please refer to python doc 3.6 to use this snippet")
		exit()
	#dt_ref=float(sys.argv[1]);dt_simu=float(sys.argv[2]);rel_tol=float(sys.argv[3]);abs_tol=float(sys.argv[3])
	#print("dt ref, simu, rel_tol, abs_tol = ",dt_ref, dt_simu, rel_tol, abs_tol)
	#if math.isclose(dt_ref,dt_simu, rel_tol=rel_tol abs_tol=abs_tol):
	exp_ref=float(sys.argv[1])
	exp_simu=float(sys.argv[2])
	abs_tol=float(sys.argv[3])
	print("Reference exponant=",exp_ref)
	print("Simulation exponant=",exp_simu)
	print("abs_tol=",abs_tol)
	if math.isclose(exp_ref,exp_simu, abs_tol=abs_tol):
		print("********************************************")
		print("* => OK Simulation is physically validated *")
		print("********************************************")
	else:
		print("********************************************************************************************************************************")
		print("* => NOK Simulation fails physically failed, the deviation of the result is too important to validate the benchmark physically *")
		print("********************************************************************************************************************************")

if __name__ == "__main__":
    main()

