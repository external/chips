Presentation
------------

Smilei is a Particle-In-Cell code for plasma simulation. Open-source, collaborative, user-friendly and designed for high performances on super-computers, it is applied to a wide range of physics studies: from relativistic laser-plasma interaction to astrophysics.

**Technical information**


* website : https://smileipic.github.io/Smilei/index.html
* Scientific domain : laser-plasma
* Language : C/C++
* Parallelism : MPI + OpenMP
* GPU acceleration : No  
* Scalability : high
* Vectorization: high


Compilation and simulation
--------------------------

**Download**

Sources are available at: https://github.com/SmileiPIC/Smilei

For the test, we will use a specific release. To download this release, run:

```
./download.sh
```

**Compile**

Information (if needed) about how to compile the application. 

For instance:

Compile the code using:
```
source machines/occigen-bdw/env
./compile.sh
```

`machines/occigen-bdw/env` contains the information for compilation (module load gcc openmpi lapack hdf5 ...)

**Run and validate the simulation**

For each test case, given in a separate folder (e.g. testcase_small), you can find three scripts:

*  prepare.sh: prepare the simulation (move data to the right location, recompile some minor changes, ...)
*  run.sh : run the application and print out the evaluated metric
*  validate.sh: validation of the simulation on a scientific point of view

For running and validating the simulation, one should be able to do:
```
cd testcase_XXX
./prepare.sh
./run.sh
./validate.sh
```
And getting no error code returned.
Those steps can also be used in a batch file for running the simulation using a job scheduler.
