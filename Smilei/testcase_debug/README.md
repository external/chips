Debug test case
---------------

The debug test case is here to validate that your compilation worked and that you can run the binary.
It should run less than 1 minute on a single node, can be run using MPI+OpenMP.
