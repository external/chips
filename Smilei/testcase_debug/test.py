# ----------------------------------------------------------------------------------------
# 					Thermal plasma 3D
# ----------------------------------------------------------------------------------------

import math as m
import numpy as np

c = 299792458
lambdar = 1e-6
wr = 2*m.pi*c/lambdar

Te   = 100./511.   				# electron & ion temperature in me c^2
Ti   = 10./511.   				# electron & ion temperature in me c^2

n0  = 1.

lambdap = 2*m.pi/n0

Lde = m.sqrt(Te)					# Debye length in units of c/\omega_{pe}

cell_length = [0.5*Lde,0.5*Lde,0.5*Lde]

dt  = 0.95 * cell_length[0]/m.sqrt(3.)		# timestep (0.95 x CFL)

# Small patches that can fit in cache
cells_per_patch = [8,8,8]

# 24 cores per sockets so at least 48 patches per node to fill all cores 
patches_per_node = [8,8,8]

# Number of nodes
nodes = [1,1,1]

# Number of patches
patches = [nodes[i]*patches_per_node[i] for i in range(3)]

# grid length
grid_length = [cells_per_patch[i]*patches[i]*cell_length[i] for i in range(3)]

# Simulation time 
simulation_time  = 120*dt			

particles_per_cell = 64

position_initialization = 'regular' 

def n0_(x,y,z):
   return n0


Main(
    geometry = "3Dcartesian",
    
    interpolation_order = 2,
    
    timestep = dt,
    simulation_time = simulation_time,
    
    cell_length  = cell_length,
    grid_length = grid_length,
    
    number_of_patches = patches,
    
    EM_boundary_conditions = [ ["periodic"] ],
    
    print_every = 10,

    random_seed = smilei_mpi_rank
)


Vectorization(
   mode = "on"
)


LoadBalancing(
    every = 20,
    initial_balance = False,
    cell_load = 1.,
    frozen_particle_load = 0.1
)

Species(
    name = "proton",
    position_initialization = position_initialization,
    momentum_initialization = "mj",
    particles_per_cell = particles_per_cell, 
    c_part_max = 1.0,
    mass = 1836.0,
    charge = 1.0,
    charge_density = n0,
    mean_velocity = [0., 0.0, 0.0],
    temperature = [Te],
    pusher = "boris",
    boundary_conditions = [
    	["periodic", "periodic"],
    	["periodic", "periodic"],
    	["periodic", "periodic"],
    ],
)
Species(
    name = "electron",
    position_initialization = position_initialization,
    momentum_initialization = "mj",
    particles_per_cell = particles_per_cell, 
    c_part_max = 1.0,
    mass = 1.0,
    charge = -1.0,
    charge_density = n0,
    mean_velocity = [0., 0.0, 0.0],
    temperature = [Ti],
    pusher = "boris",
    boundary_conditions = [
    	["periodic", "periodic"],
    	["periodic", "periodic"],
    	["periodic", "periodic"],
    ],
)

#Checkpoints(
#    dump_step = 0,
#    dump_minutes = 0.0,
#    exit_after_dump = False,
#)

#DiagFields(
#    every = 10
#)

#DiagScalar(every = 10)

#DiagPerformances(
#    every = 10,
#)

