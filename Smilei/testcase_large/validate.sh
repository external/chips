#!/bin/bash
set -e

python $(dirname $0)/validate.py
if [ `tail -n 1 results-Smilei-large/scalars.txt | awk '{print $4}'`  == "1.8759340122e+06" ] 
  then 
    echo "Validation passed"
  else 
    echo "Validation failed"
    exit 1
fi
cat results-Smilei-large/large_test.out | grep "Time in time loop "
