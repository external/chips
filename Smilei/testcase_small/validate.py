import happi 
import os
import shutil

# define the name of the directory to be created
path = "./results-Smilei-small"

try:
    os.mkdir(path)
except OSError:
    print ("Creation of the directory %s failed" % path)
else:
    print ("Successfully created the directory %s " % path)

S=happi.Open("./small_output")
S.Scalar("Utot").plot(saveAs="./results-Smilei-small/result.png")

shutil.copy2('./small_output/scalars.txt', './results-Smilei-small/')
shutil.copy2('./small_test.out', './results-Smilei-small/')
