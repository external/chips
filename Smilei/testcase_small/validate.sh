#!/bin/bash
set -e

source ./env_bench
python $(dirname $0)/validate.py
if [ `tail -n 1 results-Smilei-small/scalars.txt | awk '{print $4}'`  == "4.6898134226e+05" ] 
	then 
		echo "Validation passed"
	else 
		echo "Validation failed"
		exit 1
fi
cat results-Smilei-small/small_test.out | grep "Time in time loop "

