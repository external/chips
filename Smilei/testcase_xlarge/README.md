XLarge test case
----------------

The large test case is a not so well balanced laser simulation. This test case is not using the vectorization mode of Smilei. 

It is very close to what can be done in production for this kind of simulations. 

It requires about 250GB to run.


**Prepare the test case**

This test case restarts from a checkpoint. You must define the location of the checkpoint xlarge\_checkpoint.tar file in the prepare.sh script. 
Make sure to run the prepare.sh script in your working directory before running the application.

