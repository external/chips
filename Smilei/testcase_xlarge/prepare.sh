#!/bin/bash

if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls ../machines/
    echo ""
    echo "Example: ./prepare.sh occigen-bdw"
    exit 1
fi
machine_dir="../machines/$1"

cp $machine_dir/env_bench .
cp $machine_dir/batch_xlarge.slurm .

echo "Untar the checkpoint"
tar -xvf /store/CINES/dci/SHARED/abs/Smilei/xlarge_checkpoint.tar 
