import numpy
import math
import cmath

################### 3D Laser Wakefield with envelope
dx = 0.667
dtrans = 3.5
dt = 0.9*dx
nx = 960
ntrans = 512 + 128

Lx = nx * dx
Ltrans = ntrans*dtrans
npatch_x = 64
time_start_moving_window = 0.

Main(
    geometry = "3Dcartesian",

    interpolation_order = 2,
    timestep = dt,
    simulation_time = 32000.*dt,

    cell_length  = [dx, dtrans, dtrans],
    grid_length = [ Lx,  Ltrans, Ltrans],

    number_of_patches = [npatch_x, 64, 64],

    clrw = nx/npatch_x,

    EM_boundary_conditions = [ ["silver-muller"] ],

    solve_poisson = False,
    print_every = 100, #100,

    random_seed = smilei_mpi_rank
)


MovingWindow(
    time_start = time_start_moving_window,
    velocity_x = 1.
)


LoadBalancing(
    initial_balance = False,
    every = [nx,20], #Do not start balancing before the domain is filled with plasma
    cell_load = 2.,
)

###### Laser parameters needed for density profile definition

laser_fwhm      = 84.1  # fwhm in field, not intensity - corresponds to 20fs fwhm in intensity
center_laser    = Main.grid_length[0]-2.*laser_fwhm  # the temporal center here is the same as waist position, but in principle they can differ
focus_laser     = center_laser
focus = [focus_laser, Main.grid_length[1]/2., Main.grid_length[2]/2.]


###### Density profile

upramp_length        = 2356.
plateau_length          = 1500711.
start_ramp           = Main.grid_length[0]
n0                   = 0.000631 # 1.1e18 cm^-3

longitudinal_profile = polygonal(xpoints=[start_ramp,start_ramp+upramp_length,start_ramp+upramp_length+plateau_length,start_ramp+upramp_length+plateau_length],xvalues=[0.,n0,n0,0.])
width_plasma         = (320+128)*dtrans


def my_profile(x,y,z):
        y_centered         = y-Main.grid_length[1]/2.
        z_centered         = z-Main.grid_length[2]/2.
        profile            = 0.
        if ((y_centered**2+z_centered**2)<(width_plasma/2.)**2 ):
            profile=longitudinal_profile(x,y,z)
        return profile

######## Electron Species


Species(
    name = "electron",
    position_initialization = "regular",
    momentum_initialization = "cold",
    particles_per_cell = 8,
    c_part_max = 1.0,
    ponderomotive_dynamics = True, # = this species interacts with laser envelope
    mass = 1.0,
    charge = -1.0,
    charge_density = my_profile,
    #charge_density = f,
    mean_velocity = [0.0, 0.0, 0.0],
    temperature = [0.0],
    pusher = "ponderomotive_boris", # pusher to interact with envelope
#    pusher = "boris",
    time_frozen = 0.0,
    boundary_conditions = [
       ["remove", "remove"],
       ["remove", "remove"],
       ["remove", "remove"],
    ],
)

########## Define laser envelope

waist_laser = 314.21 # 40 um
waist       = waist_laser


LaserEnvelopeGaussian3D( # linear regime of LWFA
    a0              = 3.2378,    # 15 Joule
    focus           = [focus_laser, Main.grid_length[1]/2.,Main.grid_length[2]/2.],
    waist           = waist_laser,
    time_envelope   = tgaussian(center=center_laser, fwhm=laser_fwhm),
    envelope_solver = 'explicit',
    Envelope_boundary_conditions = [ ["reflective", "reflective"],
        ["reflective", "reflective"],
        ["reflective", "reflective"], ],
)


########## Vectorization and Checkpoints


Vectorization(
    mode = "off",
    reconfigure_every = 35,
    initial_mode = "off"
)


Checkpoints(
    restart_dir="./testcase_xlarge",
    dump_step = 0,
    dump_minutes = 230,
    exit_after_dump = False,
)

CurrentFilter(
    model = "binomial",
    passes = 4,
)

######### Diags


list_fields = ['Ex','Ey','Rho','Env_A_abs','Env_Chi','Env_E_abs','Jx']

#DiagFields(
#   every = 296,
#        fields = list_fields
#)

DiagProbe(
        every = 230,
        origin = [0., Main.grid_length[1]/2., Main.grid_length[2]/2.],
        corners = [
            [Main.grid_length[0], Main.grid_length[1]/2., Main.grid_length[2]/2.]
        ],
        number = [nx],
        fields = list_fields
)


DiagProbe(
    every = 2300,
    origin = [0., 0., Main.grid_length[2]/2.],
    corners = [
           [Main.grid_length[0], 0., Main.grid_length[2]/2.],
       [0., Main.grid_length[1], Main.grid_length[2]/2.],
                  ],
    number = [nx,ntrans],
    fields = list_fields
)


DiagPerformances(
    every = 130,
)


DiagScalar(every = 45, vars=['Env_A_absMax','Env_E_absMax'])

#DiagParticleBinning(
#       deposited_quantity = "weight_charge",
#       every = 50,
#       species = ["electron"],
#       axes = [
#               ["moving_x", 0, Main.grid_length[0], nx],
#               ["px", -1, 2., 100]
#       ]
#)

def my_filter(particles):
    return (particles.px>20.)

DiagTrackParticles(
    species = "electron",
    every = 2300,
#    flush_every = 100,
    filter = my_filter,
    attributes = ["x", "y","z","px", "py", "pz","weight"]
)




