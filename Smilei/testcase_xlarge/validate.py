import happi 
import os
import shutil

# define the name of the directory to be created
path = "./results-Smilei-xlarge"

try:
    os.mkdir(path)
except OSError:
    print ("Creation of the directory %s failed" % path)
else:
    print ("Successfully created the directory %s " % path)

S=happi.Open("./xlarge_output")
rho=S.Probe(1,"Rho")
rho.plot(saveAs="./results-Smilei-xlarge/result.png",vmax=0., vmin=-0.0006)

shutil.copy2('./xlarge_test.out', './results-Smilei-xlarge/')
shutil.copy2('./xlarge_output/Probes1.h5', './results-Smilei-xlarge/')
shutil.copy2('./xlarge_output/Probes0.h5', './results-Smilei-xlarge/')
