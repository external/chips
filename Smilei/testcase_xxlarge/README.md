Test case presentation
======================

The large test case is a very well balanced plasma simulation. This test case is very well vectorized and can be tuned to scale up to a thousand of node if necessary. 

It requires about 64TB to run.


Case profile
------------

