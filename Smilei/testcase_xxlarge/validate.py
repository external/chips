import happi 
import os
import shutil

# define the name of the directory to be created
path = "./results-Smilei-large"

try:
    os.mkdir(path)
except OSError:
    print ("Creation of the directory %s failed" % path)
else:
    print ("Successfully created the directory %s " % path)

S=happi.Open("./large_output")
S.Scalar("Utot").plot(saveAs="./results-Smilei-large/result.png")

shutil.copy2('./large_output/scalars.txt', './results-Smilei-large/')
shutil.copy2('./large_test.out', './results-Smilei-large/')
