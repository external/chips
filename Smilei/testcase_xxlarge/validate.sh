#!/bin/bash
set -e

python $(dirname $0)/validate.py
tail -n 1 results-Smilei-large/scalars.txt | awk '{print $4}'

cat results-Smilei-large/large_test.out | grep "Time in time loop "
