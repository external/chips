Presentation
------------

VASP is an atomistic simulation software for the physics/chemistry of materials.
It solves the Shrödinger equation by using the density functional theory and the Hartree-Fock method, as well as other more advanced methods.

**Technical information**

* website :https://www.vasp.at 
* Scientific domain : Materials Physics 
* Language : /Fortran/CUDA
* Parallelism : MPI + CUDA
* GPU acceleration : Yes (CUDA) 
* Scalability : high
* Vectorization: high-level


Compilation and simulation
--------------------------



**Download**

Sources are available at: https://www.vasp.at
VASP is copyright-protected software. It is necessary to have an appropriate license to use VASP.



**Compile**

For instance:

Compile the code using:
```
source machines/occigen-bdw/env_bench
./compile.sh
```

`machines/occigen-bdw/env` contains the information for compilation (module load intel openmpi)

**Run and validate the simulation**

For each test case, given in a separate folder (e.g. testcase_small), you can find three scripts:

*  prepare.sh: prepare the simulation (move data to the right location, recompile some minor changes, ...)
*  run.sh : run the application and print out the evaluated metric
*  validate.sh: validation of the simulation on a scientific point of view

For running and validating the simulation, one should be able to do:
```
cd testcase_XXX
./prepare.sh
./run.sh
./validate.sh
```
And getting no error code returned.
Those steps can also be used in a batch file for running the simulation using a job scheduler.

