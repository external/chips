#!/bin/bash
if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls machines/
    echo ""
    echo "Example: ./compile.sh occigen-bdw"
    exit 1
fi
env_file="machines/$1/env_bench"

if [ ! -f $env_file ] 
	then
    echo "ERROR: $env_file not found!"
		exit 1
	else
		source $env_file	
fi

software=vasp.5.4.4
patch_name=patch.5.4.4.16052018

rm -rf vasp 
tar zxvf ${software}.tar.gz
gunzip ${patch_name}.gz
cp ${patch_name} ${software} 

mv ${software} vasp

cp machines/$1/makefile.include vasp
cd vasp
patch -p0 < ${patch_name}

make ${versions_to_build}
cd -
