Small test case presentation
----------------------------

Production vasp test case provided by Iann Gerber.

Runs with 384 MPI ranks.


VASP basically needs 4 input files for standard production runs: 

1-INCAR
2-POSCAR
3-KPOINTS
4-POTCAR

The value of NPAR, defined in the INCAR file, can be modified in order to get better parallelisation results depending on the architecture.
