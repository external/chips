#!/bin/bash
set -e
set -x
mkdir -p results-vasp
cp -a OUTCAR* results-vasp/

max_allowed_diff=00000.0001
free_energy_good=-2139.27098853
grep free energy OUTCAR | tail -n1 > filetail

file=filetail
free_energy=$(grep 'free  energy   TOTEN' filetail| awk  {'print $6'})

echo $free_energy

echo $free_energy_good $free_energy $max_allowed_diff| awk '{if (sqrt(($1 - $2)^2) > $3) print "ERROR free energyTOTEN differs more than "$3; else print "OK accurate Total energy"}'
echo $free_energy_good $free_energy $max_allowed_diff| awk '{if (sqrt(($1 - $2)^2) > $3) print "ERROR free energyTOTEN differs more than "$3; else print "OK accurate Total energy"}' >> exitornot

# linked to set -e, if the grep doesn't work, the script will fail
grep "OK accurate Total energy" exitornot
rm exitornot
