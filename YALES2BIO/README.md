Presentation
------------

YALES2BIO is a red blood flow solver. It is used for the analysis of medical devices in contact with blood such as flow diverters, Ventricular Assist Devices, extra corporal circulation, artificial heart and valves, cytometers among others. Optimizing such devices requires dealing with both macroscopic (pressure loss, residence time, wall shear stress, turbulence) and microscopic (blood cells deformation and interaction with other cells or solid boundaries) features.


**Technical information**

* website : https://imag.umontpellier.fr/~yales2bio/index.html 
* Scientific domain : haemodynamics 
* Language : Fortran
* Parallelism : MPI + OpenMP
* GPU acceleration : No  
* Scalability : good
* Vectorization: poor


Compilation and simulation
--------------------------

Here we describe the different phases from the download to the validation of the simulation.

**Download**

The sources are available in a tarball and correspond to a stable release. To un-tar this release, run  

```
export TARBALL_PATH=path/to/yales2bio-devel/tarball (on occigen TARBALL_PATH=$SCRATCHDIR/abs_projects/)
./download.sh
```
The path of the tarball is given by the environement variable TARBALL\_PATH, exported in the .

**Compile**

To compile the code, several librairies are needed:  
* hdf5 in sequential (works with the 1.8.17 version)
* parmetis
* scotch

Then, a Makefile.in is needed. For IRENE, OCCIGEN and JEAN-ZAY platforms, this file is already made. For another platform one has to write it. An example can be found in machines/occigen-bdw/occigen.Makefile.in.  

In machines/machineName/env, the user needs to change the path of the environment variable YALES2\_HOME. Then, the user can compile the code as it is described in the following.

Compile the code using:
```
source machines/machineName/env
./compile.sh
```

Example:
```
source machines/occigen-bdw/env
./compile.sh
```

`machines/occigen-bdw/env` contains the information for compilation (module load gcc openmpi lapack hdf5 ...)

**Run and validate the simulation**

For each test case, given in a separate folder (e.g. testcase_small), you can find three scripts:

*  prepare.sh: prepare the simulation (move data to the right location, recompile some minor changes, ...)
*  run.sh : run the application and print out the evaluated metric
*  validate.sh: validation of the simulation on a scientific point of view

For running and validating the simulation, one should be able to do:
```
cd testcase_XXX
./prepare.sh
./run.sh
./validate.sh
```
And getting no error code returned.
Those steps can also be used in a batch file for running the simulation using a job scheduler.




