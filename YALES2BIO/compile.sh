#!/bin/bash

####################################
# Source the right environment
####################################
if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls machines/
    echo ""
    echo "Example: ./compile.sh occigen-bdw"
    exit 1
fi
env_file="machines/$1/env_bench"

if [ ! -f $env_file ]
        then
    echo "ERROR: $env_file not found!"
                exit 1
        else
                source $env_file
fi

####################################
# TEST THE ENVIRONMENT
####################################
echo "#########################################"
echo "CHECKING FOR HOSTTYPE Makefile.in (Y2B):"
if [ -f $YALES2_HOME/platforms/$YALES2_HOSTTYPE.Makefile.in ]; then
   if [ "$(ls -A $YALES2_HOME/src/main 2>/dev/null)" ]; then
      echo ">> Makefile.in exists for $YALES2_HOSTTYPE: SUCCESSFUL"
   fi
else
   echo ">> Makefile.in does not exist for $YALES2_HOSTTYPE: FAILED"
   echo ">> Please create a $YALES2_HOSTTYPE.Makefile.in in yales2bio-devel/platforms"
   exit
fi
echo "CHECKING THE MODULES (Y2B):"
module list

####################################
# GO TO YALES2BIO DIRECTORY
####################################
cd $YALES2_HOME/src

####################################
# RESET
####################################
echo "CHECKING CLEANING: First test"
make clean 1>00.makeClean.out 2>00.makeClean.err
found=`cat 00.makeClean.err | grep Error`
if [ -z "$found" ]; then
   echo ">> Make clean: SUCCESSFUL"
else
   echo ">> Make clean: FAILED"
   echo ">> Please look at the $YALES2_HOME/src/00.makeClean.out file"
fi

####################################
# COMPILING
####################################
echo "COMPILING: it can take several minutes"
#make -j 5 
make -j 5 1>01.make.out 2>01.make.err
echo "COMPILING: end"

####################################
# TEST THE COMPILATION
####################################
echo "CHECKING THE COMPILATION (Y2B):"
if test -f "$YALES2_HOME/lib_optim/libyales2main.a"; then
   echo ">> Compilation: SUCCESSFUL"
else
   echo ">> Compilation: FAILED"
   echo ">> Please look at the $YALES2_HOME/src/01.make.out "
   echo ">> and $YALES2_HOME/src/01.make.err files"
   exit
fi
echo "#########################################"
