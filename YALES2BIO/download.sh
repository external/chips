####################################
# CLONE THE YALES2BIO REPO
####################################
#git clone git@gitlab.coria-cfd.fr:yales2bio-devel/yales2bio-devel.git 1>/dev/null 2>/dev/null

####################################
# UNTAR THE YALES2BIO REPO
####################################
if [[ $HOSTNAME = *"occigen"* ]]; then
  TARBALL_PATH='/store/CINES/dci/SHARED/abs/yales2bio'
fi
tar xvf $TARBALL_PATH/yales2bio-devel.tar.gz -C ./ 1>/dev/null 2>/dev/null

####################################
# TEST THE DOWNLOAD
####################################
echo "##########################"
echo "CHECKING THE DOWNLOAD (Y2B):"
if [ "$(ls -A ./yales2bio-devel/src/main 2>/dev/null)" ]; then
     echo ">> main directory is not empty - DOWNLOAD SUCCESSFUL"
     download=1
else
     echo ">> main directory is empty - DONWLOAD FAILED"
     download=0
fi 
