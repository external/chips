Test case presentation
======================

This test case simulates the 1024 red blood cells (RBCs) in a shear flow.  
Size of the channel: 201x7.935x30.015 µm^3 
Shear rate: 1250 s^-1

Case profile
------------
This test case simulation was performed on Occigen on 32 broadwell node (64GB).
