####################################
# CLEAN THE FOLDER
####################################
rm *.err *.log
rm -rf restart

####################################
# Source the right environment
####################################
if [ -z "$1" ]
  then
    echo "Please provide the targeted machine from:"
    ls machines/
    echo ""
    echo "Example: ./prepare.sh occigen-bdw"
    exit 1
fi
env_file="../machines/$1/env_bench"

if [ ! -f $env_file ]
        then
    echo "ERROR: $env_file not found!"
                exit 1
        else
                source $env_file
fi

####################################
# DELETE AND CREATE DUMP DIR
####################################
rm -rf dump
mkdir dump

####################################
# COMPILE THE TEST CASE
####################################
rm makeSmallCase.out makeSmallCase.err 2>/dev/null
make veryclean 1>/dev/null 2>/dev/null
make 1>makeSmallCase.out 2>makeSmallCase.err

####################################
# COPY THE BATCH FILE
####################################
rm *.batch
cp ../machines/$1/env_bench ./
cp ../machines/$1/testcase_big.batch ./

####################################
# COPY THE RESTART FOLDER
####################################
cp -r $TESTCASE_DIR/testcase_big_restart ./restart

####################################
# TEST THE CASE COMPILATION
####################################
echo "CHECKING THE CASE COMPILATION:"
if test -f "rbc_shear_30microns"; then
   echo ">> Case compilation: SUCCESSFUL"
else
   echo ">> Case compilation: FAILED"
fi
echo "################################"
   

