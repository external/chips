!-----------------------------------------------------------------------------------
! rbc_shear_30microns program
!-----------------------------------------------------------------------------------

!=================================================================
program main

      use yales2_m

      implicit none

      ! ------------------------
      character(len=LEN_MAX) :: inputfile
      ! ------------------------

      inputfile = "rbc_shear_30microns.in"

      ! ------------------------
      ! init
      call init_yales2(inputfile,initmpi=.true.)

      ! ------------------------
      ! temporal loop
      call temporal_loop()

      ! ------------------------
      ! restart if needed
      do while (solver%reset_and_restart)
         call destroy_yales2(destroympi=.false.)
         call init_yales2(inputfile,initmpi=.false.)
         call temporal_loop()
      end do

      ! ------------------------
      if (myrank==master) then
         call print_message("Test passed successfully.",1)
      end if
      ! ------------------------
      ! destroy
      call destroy_yales2(destroympi=.true.)

end program main
!=================================================================

!=================================================================
subroutine initialize_data()

      use yales2_m
      use IFPORT

      implicit none

      ! ------------------------
      type(grid_t), pointer :: grid
      ! ------------------------
      type(pt_set_t), pointer :: pt_set
      type(pt_data_t), pointer :: initial_x_ptr,initial_indic_ptr
      integer :: nset,npt_set
      logical :: found
      ! ------------------------

      grid => solver%first_grid

      pt_set => grid%first_pt_set
      npt_set = count_pt_set(grid%first_pt_set)

      ! loop on pt set
      do nset=1,npt_set

         ! usefull pt data 
         call find_pt_data(pt_set%first_pt_data,"INITIAL_X",initial_x_ptr,resfound=found)
         if (.not.found) then
             call register_pt_data("INITIAL_X",PT_DATATYPE_REAL_VECTOR,pt_set,dump=.true.,restart=.true.,&
                                   prescribed_at_injection=.false.,new_ptr=initial_x_ptr,warn_if_exists=.false.)
         else
             initial_x_ptr%dump = .true.
             initial_x_ptr%restart = .true.
         end if

         call find_pt_data(pt_set%first_pt_data,"INITIAL_INDIC",initial_indic_ptr,resfound=found)
         if (.not.found) then
             call register_pt_data("INITIAL_INDIC",PT_DATATYPE_REAL_SCALAR,pt_set,dump=.true.,restart=.true.,&
                                   prescribed_at_injection=.false.,new_ptr=initial_indic_ptr,warn_if_exists=.false.)
         else
             initial_indic_ptr%dump = .true.
             initial_indic_ptr%restart = .true.
         end if
         ! copy the older position
         ! next
         pt_set => pt_set%next
      end do


end subroutine initialize_data
!=================================================================

!=================================================================
subroutine temporal_loop_preproc()

      use yales2_m

      implicit none

      ! ------------------------
      ! ------------------------


end subroutine temporal_loop_preproc
!=================================================================

!=================================================================
subroutine temporal_loop_postproc()

      use yales2_m

      implicit none

      ! ------------------------
      ! ------------------------

end subroutine temporal_loop_postproc
!=================================================================
