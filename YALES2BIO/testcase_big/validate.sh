#####################################################################
set -e
# VALIDATE THE SIMULATION
#####################################################################
source env_bench
if grep -q "Test passed successfully." yales2bio_testcase_big.log; then 
   python validate.py
   mkdir -p results
   cp yales2bio_testcase_big.* ./results
   mv dump ./results
   echo "TEST HAS PASSED: SUCCESSFUL"
else
   echo "TEST HAS NOT PASSED: FAIL" 
   exit 1
fi

