Small test case presentation
----------------------

This test case simulates the flow of 60 red blood cells (RBCs) in a channel.  
Size of the channel: 201x7.935x30.015 µm^3 
Velocity of the flow: 0.08m/s

Case profile
------------
A profiling of a small YALES2BIO test case performed on Occigen on 1 broadwell node (64GB) is available in this folder:
`aps_report_20200116_161556.html"

It has been generated using Intel APS (infos: https://software.intel.com/sites/products/snapshots/application-snapshot/) 
