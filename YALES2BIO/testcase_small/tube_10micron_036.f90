!-----------------------------------------------------------------------------------
! rbc_multiglobules_80G program
!-----------------------------------------------------------------------------------

!=================================================================
program main

      use yales2_m

      implicit none

      ! ------------------------
      character(len=LEN_MAX) :: inputfile
      ! ------------------------

      inputfile = "tube_10micron_036.in"

      ! ------------------------
      ! init
      call init_yales2(inputfile,initmpi=.true.)

      ! ------------------------
      ! temporal loop
      call temporal_loop()

      ! ------------------------
      ! restart if needed
      do while (solver%reset_and_restart)
         call destroy_yales2(destroympi=.false.)
         call init_yales2(inputfile,initmpi=.false.)
         call temporal_loop()
      end do

      ! ------------------------
      if (myrank==master) then
         call print_message("Test passed successfully.",1)
      end if
      ! ------------------------
      ! destroy
      call destroy_yales2(destroympi=.true.)

end program main
!=================================================================

!=================================================================
subroutine initialize_data()

      use yales2_m
      use IFPORT

      implicit none

      ! ------------------------
      ! fluid variables
      type(grid_t), pointer :: grid
      type(inputfile_t), pointer :: inputfile
      type(el_grp_t), pointer :: el_grp
      type (data_t), pointer :: u_ptr
      type (r2_t), pointer :: u
      integer :: n,i
      ! ------------------------

      inputfile => solver%first_inputfile
      grid => solver%first_grid

      call find_data(grid%first_data,"U",u_ptr)
      call copy_data_to_all_bnd(u_ptr,grid)
 
      ! u initialization
      do n=1,grid%nel_grps
         el_grp => grid%el_grps(n)%ptr
         u      => u_ptr%r2_ptrs(n)%ptr
         do i=1,el_grp%nnode
            u%val(1:grid%ndim,i) = 0.0_WP
            u%val(1,i) = 0.008_WP
         end do
      end do

end subroutine initialize_data
!=================================================================

!=================================================================
subroutine temporal_loop_preproc()

      use yales2_m

      implicit none

      ! ------------------------
      type(grid_t), pointer :: grid
      type(data_t), pointer :: velocity_source_ptr
      type(r2_t), pointer :: velocity_source
      type(el_grp_t), pointer :: el_grp
      integer :: n,i
      ! ------------------------

      ! pointers
      grid => solver%first_grid
      call find_data(grid%first_data,"VELOCITY_SOURCE",velocity_source_ptr)
      ! velocity source
      do n=1,grid%nel_grps
         el_grp => grid%el_grps(n)%ptr
         velocity_source => velocity_source_ptr%r2_ptrs(n)%ptr
         do i=1,el_grp%nnode
            velocity_source%val(1,i) = 187.5_WP
            velocity_source%val(2:3,i) = 0.0_WP
         end do
      end do


end subroutine temporal_loop_preproc
!=================================================================

!=================================================================
subroutine temporal_loop_postproc()

      use yales2_m

      implicit none

      ! ------------------------
      ! ------------------------

end subroutine temporal_loop_postproc
!=================================================================
