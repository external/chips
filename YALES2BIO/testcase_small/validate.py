#!/usr/bin/python
import argparse
import os
import yaml
import sys
import csv
from pprint import pprint
import re
import numpy as np

def main():
    #Reference values
    time_ref = 0.00012629592964
    dt_ref = 5.9664542845*10**(-7)
    dt_force_ref = 5.9664542845*10**(-7)

    #Get the log file
    log_file = 'yales2bio_testcase_small.log'
    with open(log_file) as y2b_log:
        data = y2b_log.read()
        groups = re.findall(r'(\bBegin\s+[a-z0-9]+.*?)(?=\bBegin|\Z)', data, flags=re.DOTALL|re.MULTILINE)

        #Retrieve the info of the last iteration
        last_group = groups[len(groups)-1]
        split = last_group.split('\n')
        time = float(split[2].split('>')[2].split('time')[1]) 
        dt = float(split[3].split('>')[2].split('dt')[1])
        dt_force = float(split[8].split('>')[2].split('dt_force')[1])

        err_time = np.abs(time_ref-time)/time_ref
        err_dt = np.abs(dt_ref-dt)/dt_ref
        err_dt_force = np.abs(dt_force_ref-dt_force)/dt_force_ref

        if (err_time*100.0<10.0) and (err_dt*100.0<10.0) and (err_dt_force*100.0<10.0):
            print("The error values are less than 10%")
            print("Computation is okay")
        else:
            print("One error value is greater than 10%")
            print("Computation fail")
            print("time error: ",err_time)
            print("dt error: ",err_dt)
            print("dt_force error: ",err_dt_force)

    y2b_log.close()

if __name__ == "__main__":
    main()


