#####################################################################
set -e
# VALIDATE THE SIMULATION
#####################################################################
source env_bench
if grep -q "Test passed successfully." yales2bio_testcase_small.log; then 
   echo "TEST HAS PASSED: SUCCESSFUL"
   python validate.py
   mkdir results
   cp yales2bio_testcase_small.* ./results
   mv dump ./results
else
   echo "TEST HAS NOT PASSED: FAIL" 
   exit 1
fi
