Description:
============

Presentation
------------

This template is just here to show the way that we should present the README for a given application. 

Here I describe that this template is not performing any scientific simulation, and uses no specific library.

Technical information:
----------------------

* website : http://this.url.doesnot.work 
* Scientific domain : none 
* Language : C/C++/Fortran/Python
* Parallelism : MPI + OpenMP
* GPU acceleration : Yes (CUDA) 
* Scalability : high
* Vectorization: poor


Compilation and simulation:
===========================

Here we describe the different phases from the download to the validation of the simulation.

Download:
---------

Information (if needed) about how to get the sources.

For instance:
```
./download.sh
```

Compile:
--------

Information (if needed) about how to compile the application. 

For instance:

Compile the code using:
```
./compile.sh occigen-bdw
```

`machines/occigen-bdw/env` contains the information for compilation (module load gcc openmpi lapack hdf5 ...)

You can create your own machine directory under `machines` to define the appropriate environment.

Run and validate the simulation:
--------------------------------

For each test case, given in a separate folder (e.g. testcase_small), you can find three scripts:

*  prepare.sh: prepare the simulation (move data to the right location, recompile some minor changes, ...)
*  run.sh : run the application and print out the evaluated metric
*  validate.sh: validation of the simulation on a scientific point of view

For running and validating the simulation, one should be able to do:
```
cd testcase_XXX
./prepare.sh occigen-bdw
./run.sh
./validate.sh
```
And getting no error code returned.
Those steps can also be used in a batch file for running the simulation using a job scheduler.




