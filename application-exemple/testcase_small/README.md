Test case presentation
======================

The small test case of the template application is doing nothing but watching videos on Youtube all day long.
It uses no DFT method, nor spectral method or anything else. The FFTW is widely not use for this case.


Case profile
------------

A profiling of a small specfem3D test case performed on Occigen on 1 haswell node (64GB) is available in this folder:
`profile_occigen-hsw.html"

It has been generated using Intel APS (infos: https://software.intel.com/sites/products/snapshots/application-snapshot/) 
