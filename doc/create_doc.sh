#!/bin/bash 

cat >source/index.rst <<EOL
.. CHiPS documentation documentation master file, created by
   sphinx-quickstart on Thu Apr 16 11:09:09 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CHiPS' documentation!
================================

Contents:

.. toctree::
   :maxdepth: 2

EOL


for elt in `find ../ -maxdepth 1 -type d -printf '%f\n' | grep -E -v '\.|exemple|doc' | sort`
do 

    echo "   $elt/$elt.md" >> source/index.rst
    mkdir -p source/$elt
    OUTPUTFILE=source/$elt/$elt.md

    echo "$elt" > $OUTPUTFILE
    echo "=======" >> $OUTPUTFILE
    cat ../$elt/README.md >> $OUTPUTFILE 
    for testcasedir in `ls ../$elt | grep testcase*`
    do
       echo "" >> $OUTPUTFILE
       cat ../$elt/$testcasedir/README.md >> $OUTPUTFILE
    done
done

cat >>source/index.rst <<EOL

Indices and tables
==================

* :ref:\`genindex\`
* :ref:\`modindex\`
* :ref:\`search\`
EOL

make html
echo ""
echo "Open build/html/index.html with your web browser to read the documentation."
echo ""
