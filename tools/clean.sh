#!/bin/bash
ls ${SCRATCHDIR}/abs/${CI_COMMIT_SHORT_SHA}/
if [[ -n ${CI_COMMIT_SHORT_SHA} && -d "${SCRATCHDIR}/abs/${CI_COMMIT_SHORT_SHA}" ]]; then
        echo "Cleaning..."
        echo "CI_COMMIT_SHORT_SHA= ${CI_COMMIT_SHORT_SHA}"
        cd ${SCRATCHDIR}/abs/${CI_COMMIT_SHORT_SHA}/
        echo "pwd " `pwd`
        rm -rf Abinit/abinit-8.10.3 Abinit/*.tar.gz  DYNAMICO/DYNAMICO DYNAMICO/XIOS GYSELAX/gysela  Gromacs/gromacs/ Gromacs/gromacs-2020.1 Gromacs/gromacs-2020.1.tar.gz MUMPS/MUMPS_5.2.1 MUMPS/mumps_5.2.1.orig.tar.gz NEMO/release-4.0 RAMSES/ramses RAMSES/testcase_medium/input/ RAMSES/testcase_medium/output_00001/ RAMSES/testcase_medium/PROD/ RAMSES/testcase_small/DEBUG Smilei/Smilei YALES2BIO/yales2bio-devel YALES2BIO/testcase_big/dump
        echo "...End cleaning."
        echo "Number of files remaining in ${SCRATCHDIR}/abs/${CI_COMMIT_SHORT_SHA} = "; find . | wc -l
else
        echo "Cleaning not performed"
        echo "\${SCRATCHDIR}/abs/\${CI_COMMIT_SHORT_SHA}/=" ${SCRATCHDIR}/abs/${CI_COMMIT_SHORT_SHA}
fi
